package controller;

import model.data_structures.*;
import model.logic.OLDManager;
import model.vo.Station;
import model.vo.Trip;



/**
 * concexion entre la logica y el view
 * @author Andres Forero, Bryan Cuta
 *
 */
public class Controller {
	
    private static OLDManager manager = new OLDManager();

  
    
    
    public void load(String dataTrips, int tripsIdFile, String dataStations, int stationsIdFile){
    	manager.C1cargar(dataTrips, tripsIdFile, dataStations, stationsIdFile);
    }
    
    
    
    public void loadTrips(String rutaTrips, int tripsIdFile){
    	manager.C1cargarTrips(rutaTrips, tripsIdFile);
    }         
    
    
    public void loadStations(String rutaEstaciones, int estacionesIdFile){
    	manager.C1cargarStations(rutaEstaciones, estacionesIdFile);
    }   
    
    
    public void loadBikes()
    {
    	manager.loadBikes();
    }
    
    
    public Lista<Trip> getListaTrips()
    {
    	return manager.listaTrips;
    }
    
    
    public Lista<Station> getListaEstaciones()
    {
    	return manager.listaEstaciones;
    }
    
    
    public void  resetTrips()
    {
    	manager.resetTrips();
    }
    
    
    public void resetStations()
    {
    	manager.resetStations();
    }
    
    public void resetBikes() {
    	//TODO
    }
    
    public void resetAll()
    {
    	manager.reset();
    }
    
    
}
