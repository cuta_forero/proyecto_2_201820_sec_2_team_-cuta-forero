package model.logic;

import api.IManager;
import model.data_structures.Lista;
import model.data_structures.Nodo;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.logic.Sorter.Merge;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

public class OLDManager implements IManager {
	
	public static final double R = 6371000.0; // radio de la tierra en metros
	
	//Ruta del archivo de datos 2017-Q1	
	public static final String TRIPS_Q1 = "./docs/data/Divvy_Trips_2017_Q1.csv";
	
	//Ruta del archivo de trips 2017-Q2		
	public static final String TRIPS_Q2 = "./docs/data/Divvy_Trips_2017_Q2.csv";
		
	//Ruta del archivo de trips 2017-Q3	
	public static final String TRIPS_Q3 = "./docs/data/Divvy_Trips_2017_Q3.csv";
			
	//Ruta del archivo de trips 2017-Q4	
	public static final String TRIPS_Q4 = "./docs/data/Divvy_Trips_2017_Q4.csv";
		
	//Ruta del archivo de stations 2017-Q1-Q2	
	public static final String STATIONS_Q1_Q2 = "./docs/data/Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4	
	public static final String STATIONS_Q3_Q4 = "./docs/data/Divvy_Stations_2017_Q3Q4.csv";
	
	
	
		
	
	
	
	/**
	 * lista de vaijes cargados segun el usuario
	 */
	public Lista<Trip> listaTrips = new Lista<>();
	
	
	
	/**
	 * lista de las estaciones cargadas segun el usuario
	 */
	public Lista<Station> listaEstaciones = new Lista<Station>();
	
	
	
	/**
	 * lista de bicicletas generados segun los datos de los trips
	 *
	 */
	public Lista<Bike> listaBykes = new Lista<>();
	
		
	
	
	//--------------------------------------------------------------------------------------------------//
	//										PARTE A: Bryan Cuta														//
	//--------------------------------------------------------------------------------------------------//
	

		public Queue<Trip> A1ViajesEnPeriodoDeTiempo(Date fechaInicial, Date fechaFinal) {

			Nodo<Trip> iter = listaTrips.getFirstNodo();

			boolean comprobante = false;

			Queue<Trip> rta2 = new Queue<>(); 

			while (iter.getNext() != null && comprobante == false){

				Trip act = iter.getitem();

				if (act.getStopTime().compareTo(fechaFinal) <= 0 ){

					if(act.getStartTime().compareTo(fechaInicial) >= 0){ 
						rta2.queue(act);

					}else {comprobante = true;}
				}

				iter =  iter.getNext();
			}

			return rta2;
		}

		public Lista<Bike> A2BicicletasOrdenadasPorNumeroViajes(Date fechaInicial, Date fechaFinal) {


			Lista<Bike> listaBicicletasRango = loadBikes(fechaInicial, fechaFinal);


			Bike[] data = new Bike[listaBicicletasRango.size()];    		
			int i = 0;    		
			while(!listaBicicletasRango.isEmpty() ){
				data[i] = listaBicicletasRango.removeFirst();
				i++;
			}
			listaBicicletasRango.deleteList();    		
			Merge.sort(data,3,1);//ordena por distancia
			Merge.sort(data,2,1);//ordena por numero de viajes

			for (Bike bk : data) {    			
				listaBicicletasRango.addEnd(bk);
			}
			data = null;

			return listaBicicletasRango;

		}

		public Lista<Trip> A3ViajesPorBicicletaPeriodoTiempo(int bikeId, Date fechaInicial, Date fechaFinal) {
			Lista<Trip> listaTrips = new Lista<>();
			Queue<Trip> acotador = A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
			
			Nodo<Trip> iter = acotador.getFirstNodo();
			while (iter.getNext() != null){
				Trip act = iter.getitem();
				if(act.getBikeId() == bikeId){
					listaTrips.addEnd(act);
				}
				
				iter = iter.getNext();
			}
				
			return listaTrips;
		}

		public Lista<Trip> A4ViajesPorEstacionFinal(int endStationId, Date fechaInicial, Date fechaFinal) {
			Lista<Trip> listEstacionFinal = new Lista<>();
			Queue<Trip> acotador = A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
			Nodo<Trip> iter = acotador.getFirstNodo();
			while (iter.getNext() != null ){
				Trip act = iter.getitem();
				if(act.getEndStationId() == endStationId){
					listEstacionFinal.addEnd(act);
				}
				iter = iter.getNext();
			}
			
			return listEstacionFinal;
		}
    
	
	
    
    
    
   
    
    
    
    
    //--------------------------------------------------------------------------------------------------//
  	//										PARTE B: Andres Forero														//
  	//--------------------------------------------------------------------------------------------------//
    
    public Queue<Station> B1EstacionesPorFechaInicioOperacion(Date fechaComienzo) {
		
    	Queue<Station> queue = new Queue<>();// cola respuesta
    	
		Nodo<Station> iterador = listaEstaciones.getFirstNodo(); // iterador sobre la lista estaciones
		
		while(iterador.getNext() != null)
		{
			Station actual = iterador.getitem();
			//si la fecha de incio de operacion esta despues
			if(actual.getStartDate().after(fechaComienzo) || actual.getStartDate().equals(fechaComienzo))
			{
				queue.queue(actual); // agregar la estacion ala cola
			}
			iterador = iterador.getNext();
		}
		
		//ordenar la cola
		
		Station[] data = new Station[queue.size()];    		
		int i = 0;    		
		while(!queue.isEmpty() ){
			data[i] = queue.removeFirst();
			i++;
		}
		queue.deleteList();  
		
		//en orden ascendente por fecha de inicio
		Merge.sort(data,3,0); 
		
		//devolver todo a la cola
		for (Station st : data) {    			
			queue.queue(st);
		}
		data = null;		
    	return queue;       
    }

    
    
    public Lista<Bike> B2BicicletasOrdenadasPorDistancia(Date fechaInicial, Date fechaFinal) {
       
    	//lista de respuesta
    	Lista<Bike> listaBicicletasRango = loadBikes(fechaInicial, fechaFinal);          
          
    	//ordenar las bicicletas encontradas
        Bike[] data = new Bike[listaBicicletasRango.size()];    		
  		int i = 0;    		
  		while(!listaBicicletasRango.isEmpty() ){
  			data[i] = listaBicicletasRango.removeFirst();
  			i++;
  		}
  		listaBicicletasRango.deleteList(); 
  		//por distancia total (3) en orden decreciente (1)
  		Merge.sort(data,3,1);
  		
  		//devolver todo al arreglo
  		for (Bike bk : data) {    			
  			listaBicicletasRango.addEnd(bk);
  		}
  		data = null;
  		
  		return listaBicicletasRango;
    	
    }

    
    
    public Lista<Trip> B3ViajesPorBicicletaDuracion(int bikeId, int tiempoMaximo, String genero) {
		
    	Lista<Trip> listaRespuesta = new Lista<>();
    	if(listaBykes.isEmpty())
    	{
    		loadBikes();
    	}
    	
    	Nodo<Bike> iterador = listaBykes.getFirstNodo();
    	Bike bike = new Bike(bikeId);
    	while(iterador.getNext()!= null)
    	{
    		Bike actual = iterador.getitem();
    		if(bikeId == actual.getBikeId())
    		{
    			bike = actual;
    			break;
    		}
    		iterador = iterador.getNext();
    	}
    	
    	
    	Nodo<Trip> iter = bike.tripsAsociados.getFirstNodo();
    	while(iter.getNext() != null)
    	{
    		Trip current = iter.getitem();
    		if(current.getTripDuration() <= tiempoMaximo && current.getGender().equalsIgnoreCase(genero))
    		{
    			listaRespuesta.addEnd(current);
    		}
    		iter = iter.getNext();
    	}
    	
    	return listaRespuesta;
       
    }

    
    
    
    public Lista<Trip> B4ViajesPorEstacionInicial(int startStationId, Date fechaInicial, Date fechaFinal) {
		
    	Lista<Trip> listaRespuesta = new Lista<>();
    	ordenarTrips(6, 0);
    	Nodo<Trip> iterador = listaTrips.getFirstNodo();
    	while(iterador.getNext() != null)
    	{
    		Trip actual = iterador.getitem();
    		if(actual.getStartStationId() == startStationId)
    		{
    			Date startTime = actual.getStartTime();
    			Date stopTime = actual.getStopTime();
    			if((startTime.after(fechaInicial) || startTime.equals(fechaInicial)) && ( stopTime.before(fechaFinal) || stopTime.equals(fechaFinal)))
    			{
    				listaRespuesta.addEnd(actual);
    			}
    		}if(actual.getStartStationId() > startStationId)
    		{
    			break;
    		}
    		iterador = iterador.getNext();
    	}
    	
    	Trip[] data = new Trip[listaRespuesta.size()];    		
		int i = 0;    		
		while(!listaRespuesta.isEmpty() ){
			data[i] = listaRespuesta.removeFirst();
			i++;
		}
		listaRespuesta.deleteList();    		
		Merge.sort(data,2,0);    		
		for (Trip trip : data) {    			
			listaRespuesta.addEnd(trip);
		}
		data = null;		
    	
    	
    	
    	
    	
    	return listaRespuesta;        
    	
    }

    
   
    
    
    //--------------------------------------------------------------------------------------------------//
  	//										PARTE C														//
  	//--------------------------------------------------------------------------------------------------//
    
	public void C1cargar(String rutaTrips, int tripsFileId, String rutaStations, int stationsFileId) {
		
		C1cargarTrips(rutaTrips, tripsFileId);
		loadStations(rutaStations);	
	}
	

	public void C1cargarTrips(String rutaTrips, int tripsIdFile) {
		loadTrips(rutaTrips, tripsIdFile);
	}

	
	public void C1cargarStations(String rutaEstaciones, int estacionesIdFile) {
		loadStations(rutaEstaciones);		
	}
	
	
	
	public Queue<Trip> C2ViajesValidadosBicicleta(int bikeId, Date fechaInicial, Date fechaFinal) {
		
		loadBikes();
//		Stack<Trip> 
		Queue<Trip> inconsistencias = new Queue<>();
		Nodo<Bike> iter = listaBykes.getFirstNodo();
		while (iter.getNext()!= null){
			Bike act = iter.getitem();
			
			if(act.getBikeId() == bikeId){
				Lista<Trip> trips = act.getTripsAsociados();
				Lista<Trip> tripsAcotados = ViajesEnPeriodoDeTiempo(trips, fechaInicial, fechaFinal);
				Stack<Trip> stackValidacion = new Stack<>();
				
				while (tripsAcotados.size() >  0){
					Trip aValidar = tripsAcotados.removeFirst();
					if(stackValidacion.isEmpty() == true){
						stackValidacion.push(aValidar);
					}
					else {
						int endTrip1 = stackValidacion.peek().getEndStationId();
						int startTrip = aValidar.getEndStationId();
						
						if (endTrip1 == startTrip){
							stackValidacion.push(aValidar);
						}
						else{
							inconsistencias.queue(stackValidacion.pop());
							inconsistencias.queue(aValidar);
							
							while (stackValidacion.isEmpty() == false ){
								stackValidacion.pop();
							}
							stackValidacion.push(aValidar);
						}
					}
					
					
				}
			}
			
			iter = iter.getNext();
		}
		
		return inconsistencias;
	}

	
	
	
	
	public Lista<Bike> C3BicicletasMasUsadas(int topBicicletas) {

		//prdena la lista de bikes(3) por duracion
		ordenarBikes(4,1);
		
		Lista<Bike> listaTop = new Lista<>();
		Nodo<Bike> iterador = listaBykes.getFirstNodo();
		
		int index = 1;
		while(iterador.getNext() != null  && index <= topBicicletas)
		{
			Bike actual = iterador.getitem();
			listaTop.addEnd(actual);
			iterador = iterador.getNext();
			index++;
		}		
		return listaTop;
	}

	
	public Lista<Trip> C4ViajesEstacion(int StationId, Date fechaInicial, Date fechaFinal) {
		Lista<Trip> list = new Lista<>();
		
		ordenarTrips(7, 0);
		ordenarTrips(6, 0);	
		
		
		Nodo<Trip> actual = listaTrips.getFirstNodo();
		while(actual.getNext() != null)
		{
			Trip current = actual.getitem();			
			boolean equalsStart =  current.getStartStationId() == StationId; 			
			boolean equalsEnd = current.getEndStationId() == StationId;
			if(equalsStart && equalsEnd)
			{
				
				boolean finEnRango = current.getStopTime().before(fechaFinal) || current.getStopTime().equals(fechaFinal);
				boolean inicioEnRango = current.getStartTime().after(fechaInicial) ||  current.getStartTime().equals(fechaInicial);
				
				if(inicioEnRango  && finEnRango)
				{
					list.addEnd(current);
				}
				
			}
			if(current.getStartStationId() > StationId || current.getEndStationId() > StationId)
			{
				break;
			}
			actual = actual.getNext();
		}
		actual = null;
		return list;
	}
	
	
	
	
	//--------------------------------------------------------------------------------------------------//
  	//										METODOS DE RESPALDO	LOGICA								//
  	//--------------------------------------------------------------------------------------------------//
	
	
	/**
	 * calcula la distancia entre dos estaciones buscadolas en la lista de estaciones
	 * 
	 * @param idStation1
	 * @param idStation2
	 * @return doube distancia 
	 */
	public  double distanciaEntreEstaciones(int idStation1, int idStation2 )
	{
		double gradLat1 = 0.0;
		double gradLong1 = 0.0;
		
		double gradLat2 = 0.0;
		double gradLong2 = 0.0;
		
		Nodo<Station> iterador = listaEstaciones.getFirstNodo();
		
		
		
		while(iterador.getNext() != null)
		{
			Station actual = iterador.getitem();
			if(actual.getStationId() == idStation1)
			{
				gradLat1 = actual.getLatitude();
				gradLong1 = actual.getLongitude();				
			}
			if(actual.getStationId() == idStation2)
			{
				gradLat2 = actual.getLatitude();
				gradLong2 = actual.getLongitude();
			}
			if(gradLat1 != 0.0 && gradLong1 != 0.0 && gradLat2 != 0.0 & gradLong1 != 0.0)
			{
				break;
			}
			iterador = iterador.getNext();			
			
		}
		
		return harvesineDistance(gradLat1*(Math.PI/180.0),(gradLong1*(Math.PI/180)), (gradLat2*(Math.PI/180)), (gradLong2*(Math.PI/180)));
	}
	
	/**
	 * 
	 * parametros de longitud y latitud en radianes
	 * 
	 * para la distancia aproximada de dos puntos sobre el globo terraqueo
	 */
	public double harvesineDistance(double latitude1, double longitude1, double latitide2, double longitude2)
	{		
		double deltaLat = Math.abs(latitide2 - latitude1);
		double deltaLong = Math.abs(longitude2 - longitude1);
		
		double a = Math.pow(Math.sin(deltaLat/2), 2) + (Math.cos(latitude1)*Math.cos(latitide2)*Math.pow(Math.sin(deltaLong/2), 2));
		
		double c = 2 * Math.atan(Math.sqrt(a)/Math.sqrt(1-a));
		
		return (R * c);
	}
	
	
	
	
	public Lista<Trip> ViajesEnPeriodoDeTiempo(Lista<Trip>list, Date fechaInicial, Date fechaFinal) {
		
		if(list.isEmpty())
			return null;
		
		
		
		Nodo<Trip> iter = list.getFirstNodo();

		boolean comprobante = false;

		Lista<Trip> rta2 = new Lista<>(); 

		while (iter.getNext() != null && comprobante == false){

			Trip act = iter.getitem();

			if (act.getStopTime().compareTo(fechaFinal) <= 0 ){

				if(act.getStartTime().compareTo(fechaInicial) >= 0){ 
					rta2.addEnd(act);

				}else {comprobante = true;}
			}

			iter =  iter.getNext();
		}

		return rta2;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//--------------------------------------------------------------------------------------------------//
  	//										METODOS DE RESPALDO	CARGA									//
  	//--------------------------------------------------------------------------------------------------//
	
	
	
	
	
	/**
	 * llena la lista listaTrips con objetos Trip obtenidos de los archivos csv respectivos 
	 * @param tripsFile ruta relativa del archivo desde la raiz del proyecto
	 * @param idFile identificador del tipo de archivo, algunos tienen las fechas con segundos, otros no
	 */
	public void loadTrips(String tripsFile, int idFile)
	{
		
		CsvParserSettings settings = new CsvParserSettings();
		
		settings.setInputBufferSize(4000000);
		
		CsvParser parser = new CsvParser(settings);
		
		parser.beginParsing(new File (tripsFile));
		
		String[] campos = parser.parseNext();
		
		boolean seconds = true;
		if(idFile == 4)
		{
			seconds = false;
		}
		
		
		while((campos = parser.parseNext()) != null)
		{
			Trip actual  = parserTrips(campos, seconds);
			listaTrips.addFirst(actual);			
			System.out.println(actual.toString());
		}
		
	}
	
	
	
	
	/**
	 * llena la lista listaEstaciones con objetos Station obtenidos del archivo csv respectivo
	 * @param stationsFile ruta relativa del archivo desde la raiz del proyecto
	 * la carga puede ser progesiva
	 */
	public void loadStations(String stationsFile){
		
		CsvParserSettings settings = new CsvParserSettings();
		CsvParser parser = new CsvParser(settings);
		
		parser.beginParsing(new File(stationsFile));
		
		String[] campos = parser.parseNext();	
				
		while ((campos = parser.parseNext()) != null)
		
		{
			Station actual = parserStations(campos, false);
			listaEstaciones.addEnd(actual);		
			System.out.println(actual.toString());			
		}	
	}
	
	
	
	
	
	
    
    
	
	/**
	 * carga la lista listaBicicletas en funcion de los datos de Trips
	 * 
	 * ahorra el tiempo de procesamiento en diferentes requerimientos
	 * 
	 */
	public void loadBikes()
	{
				
		ordenarTrips(4, 0);
		
		Nodo<Trip> iterador = listaTrips.getFirstNodo();
		
		Trip actual = iterador.getitem();
		
		int globalcounterTrips = 0;
		
		int idActual = actual.getBikeId();
		Bike newBike = new Bike(idActual);
		while(iterador.getNext() != null)
		{			
			if(actual.getBikeId() == idActual)
			{
				newBike.setTotalDistance(newBike.getTotalDistance() + distanciaEntreEstaciones(actual.getStartStationId(), actual.getEndStationId()));
				newBike.setTotalDuration(newBike.getTotalDuration() + actual.getTripDuration());
				newBike.setTotalTrips(newBike.getTotalTrips() +1);				
				newBike.tripsAsociados.addEnd(actual);
				globalcounterTrips++;				
			}else
			{
				listaBykes.addEnd(newBike);				
				idActual = iterador.getNext().getitem().getBikeId();
				newBike = new Bike(idActual);								
			}
			
			actual = iterador.getNext().getitem();		
			iterador = iterador.getNext();
		}
		
		System.out.println("\n\tSE CARGARON LOS DATOS DE LAS BICICLETAS ");	
		System.out.println("\n\t " + globalcounterTrips + "Trips y " + listaBykes.size() + " bicicletas \n");	
		
	}
	
	
	/**
	 * carga toda la informacion de las bicicletas en un rango de fechas
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 */
	public Lista<Bike> loadBikes(Date fechaInicial, Date fechaFinal )
	{
		
		Lista<Bike> bikesRango = new Lista<>();
		
		if(listaBykes.isEmpty())
		{
			loadBikes();
		}
		
		Nodo<Bike> nodoActual = listaBykes.getFirstNodo();
		while(nodoActual.getNext() != null)
		{
			Bike actual = nodoActual.getitem();
			Bike newBikeInRango = getBikeRango(actual, fechaInicial, fechaFinal);
			if(newBikeInRango.getTotalTrips() > 0 && newBikeInRango.getTotalDistance() > 0.0)
			{
				bikesRango.addEnd(newBikeInRango);
			}
			
			nodoActual = nodoActual.getNext();
		}
		
		return bikesRango;
		
	}
	
	/**
	 * calcula los datos de una bicicleta pasada por parametro SOLO en un rango dado
	 * @param bike bicicleta con datos acotados
	 * @param start
	 * @param end
	 * @return
	 */
	public Bike getBikeRango(Bike bike, Date start, Date end)
	{
		Bike b = new Bike(bike.getBikeId());
		Integer pTotalTrips = 0;
		Double pTotalDistance = 0.0;
		Integer pTotalDuration = 0;
		
		Nodo<Trip> actual = bike.tripsAsociados.getFirstNodo();
		while(actual.getNext() != null)
		{
			Trip current = actual.getitem();
			Date ini = current.getStartTime();
			Date fin = current.getStopTime();
			if((ini.after(start) || ini.equals(start)) && (fin.before(end) || fin.equals(end)) )
			{
				pTotalDistance +=  distanciaEntreEstaciones(current.getStartStationId(), current.getEndStationId());
				pTotalDuration += current.getTripDuration();
				pTotalTrips++;				
			}
			actual = actual.getNext();
		}
		
		b.setTotalTrips(pTotalTrips);
		b.setTotalDistance(pTotalDistance);
		b.setTotalDuration(pTotalDuration);
		
		return b;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * metodo para definir un objeto VOStation segun una linea con split
	 * @param campos linea spliteada de los valores de la linea
	 * @return objeto VOStation inicializado
	 */
	public Station parserStations(String[] campos, boolean seconds)
	{
		Station station;	
		
		Date onlineDate = convertDate(campos[6], seconds);
		
		station = new Station(Integer.valueOf(campos[0]), campos[1], Double.valueOf(campos[3]), Double.valueOf(campos[4]), Integer.valueOf(campos[5]), onlineDate);
		
		return station;
	}
	
	
	
	
	/**
	 * metodo para definir un objeto VOTrip segun una linea con split
	 * @param campos linea spliteada de los valores de la linea
	 * @param seconds centinela para verificar si el archivo tiene fechas con segundos o no
	 * @return objeto VOTrips inicializado
	 */
	public Trip parserTrips(String[] campos, boolean seconds) {
		
		
		Trip trip;
		String userType = campos[9];
		String gender = Trip.UNKNOWN;
		String birthYear = Trip.UNKNOWN;
		if(userType.equals("Subscriber"))
		{
			try {
				if(!campos[10].equals("") || !campos[10].equals(null))
					gender = campos[10];
				if(!campos[11].equals("") || !campos[10].equals(null))
					birthYear = campos[11];
			} catch (Exception e) {
				
			}
			
		}
		Date startDate = convertDate(campos[1], seconds);
		Date endDate = convertDate(campos[2], seconds);
		
		
		trip = new Trip(Integer.valueOf(campos[0]), startDate, endDate,
						Integer.valueOf(campos[3]), Integer.valueOf(campos[4]),
						Integer.valueOf(campos[5]), campos[6], Integer.valueOf(campos[7]), campos[8],
						userType, gender, birthYear);
		
		return trip;	
	}
	
	
	
	
	
	
	/**
	 * 
	 * convertidor de un string con formato conocido
	 * @param str cadena con la fecha
	 * @param seconds centinela para verificar si el archibo tiene fechas con segundos o no
	 * @return objeto Date 
	 */
	public static Date convertDate(String str, boolean seconds)
	{
		SimpleDateFormat formatter;
		Date date = null;
		
		formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		if(!seconds)
		{
			str += ":00";
			;
		}
		try {
			date = formatter.parse(str);
		} catch (ParseException e) {
			System.out.println("Error leyendo las fechas");
			e.printStackTrace();
		}				
		

		return date;
	}
	
	
	
    
    public void ordenarTrips(int parametro, int order)
    {
    	Trip[] data = new Trip[listaTrips.size()];    		
		int i = 0;    		
		while(!listaTrips.isEmpty() ){
			data[i] = listaTrips.removeFirst();
			i++;
		}
		listaTrips.deleteList();    		
		Merge.sort(data,parametro,order);    		
		for (Trip trip : data) {    			
			listaTrips.addEnd(trip);
		}
		data = null;		
    }
    
    public void ordenarStations(int parametro, int order)
    {
    	Station[] data = new Station[listaEstaciones.size()];    		
		int i = 0;    		
		while(!listaEstaciones.isEmpty() ){
			data[i] = listaEstaciones.removeFirst();
			i++;
		}
		listaEstaciones.deleteList();    		
		Merge.sort(data,parametro,order);    		
		for (Station st : data) {    			
			listaEstaciones.addEnd(st);
		}
		data = null;		
    }
    
    public void ordenarBikes(int parametro, int order)
    {
    	Bike[] data = new Bike[listaBykes.size()];    		
		int i = 0;    		
		while(!listaBykes.isEmpty() ){
			data[i] = listaBykes.removeFirst();
			i++;
		}
		listaBykes.deleteList();    		
		Merge.sort(data,parametro,order);    		
		for (Bike bk : data) {    			
			listaBykes.addEnd(bk);
		}
		data = null;		
    }
    
    
    

	/**
	 * resetea la lista de Estaciones
	 */
	public void resetStations()
	{
		listaEstaciones.deleteList();
	}
		
	
	
	/**
	 * resetea la lista de Viajes
	 */
	public void resetTrips()
	{
		listaTrips.deleteList();
	}
	
	
	public void resetBikes()
	{
		listaBykes.deleteList();
	}
	
	
	
	/**
	 * resetea todas las listas
	 */
	public void reset() {
		resetStations();
		resetTrips();
		resetBikes();
	}
	
	
	
	

	
	
	
}
