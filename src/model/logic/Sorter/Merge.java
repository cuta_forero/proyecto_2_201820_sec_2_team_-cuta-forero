package model.logic.Sorter;

import model.vo.VO;


/**
 * 	CLASE basada en la implementacion del mergeSort de Robert Sedgewick &  Kevin Wayne para el libro Algorithms 4th perason
 *  Clase Merge Sort sobre un estructura de VOs
 * 
 * @author Andres Forero, Bryan Cuta
 *
 * @param <E> tipo VO
 */
public class Merge<E extends VO<E>> {

   
    
   
    private static void merge(VO[] a, VO[] aux, int lo, int mid, int hi, int p, int order) {
             
        for (int k = lo; k <= hi; k++) {
            aux[k] = a[k]; 
        }
       
        int i = lo;
        int j = mid+1;
        
        for (int k = lo; k <= hi; k++) {
           if(order == 0)
           {
        	   if      (i > mid)              a[k] = aux[j++];
               else if (j > hi)               a[k] = aux[i++];
               else if (less(aux[j], aux[i],p)) a[k] = aux[j++];
               else                           a[k] = aux[i++];
           }else
           {
        	   if		(i > mid)			a[k] = aux[j++];
        	   else if	(j > hi)			a[k] = aux[i++];
        	   else if(greater(aux[j], aux[i], p)) a[k] = aux[j++];
        	   else 						a[k] = aux[i++];
           }
        }

       
    }

    
    private static void sort(VO[] a, VO[] aux, int lo, int hi, int p, int order) {
        if (hi <= lo) return;
        int mid = lo + (hi - lo) / 2;
        sort(a, aux, lo, mid, p,order);
        sort(a, aux, mid + 1, hi, p,order);
        merge(a, aux, lo, mid, hi,p, order);
    }

    
    public static void sort(VO[] a, int p, int order) {
        VO[] aux = new VO[a.length];
        sort(a, aux, 0, a.length-1, p, order);        
    }


   
    
    // v < w ?
	private static boolean less( VO<VO> v, VO w, int p) {           	
    		return v.compareTo(w, p) < 0;    	
    }
    
	//v > w ?
    private static boolean greater(VO<VO> v, VO w, int p ){
    		return v.compareTo(w,p) > 0;
    }
        
  
    
    
}
