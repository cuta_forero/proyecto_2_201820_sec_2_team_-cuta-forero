package model.vo;

import model.data_structures.Lista;

/**
 * Clase que representa un Objeto bike
 * 
 * @author Andres Forero, Bryan Cuta
 *
 */
public class Bike implements VO<Bike>{

	
	/**
	 * identificador unico de una bicicleta
	 * factor de orden total(clave)
	 */
    private Integer bikeId;
    
    
    /**
     * numero total de viajes historicamente realizados
     */
    private Integer totalTrips;
    
    
    /**
     * distancia historica total de la distancia total
     */
    private Double totalDistance;
    
    
    /**
     * numero de segundos que se ha utilizado esta bicicleta
     */
    private Integer totalDuration;
    
    
    /**
     * objetos trip asociados a esta bicicleta
     */
    public Lista<Trip> tripsAsociados;

    
    public Bike(int bikeId)
    {
    	this.bikeId = bikeId;
    	this.totalTrips = 0;
    	this.totalDistance = 0.0;
    	this.totalDuration = 0;
    	tripsAsociados = new Lista<>();
    }
    
    
    public Bike(int bikeId, int totalTrips, double totalDistance, int ptotalDuration) {
        
    	this.bikeId = bikeId;        
        this.totalTrips = totalTrips;        
        this.totalDistance = totalDistance;        
        this.totalDuration = ptotalDuration;
        tripsAsociados = new Lista<>();
    }    

    public int getBikeId() {
        return bikeId;
    }

    public int getTotalTrips() {
        return totalTrips;
    }

    public double getTotalDistance() {
        return totalDistance;
    }
    public int getTotalDuration() {
    	return totalDuration;
    }


	public void setBikeId(int bikeId) {
		this.bikeId = bikeId;
	}


	public void setTotalTrips(int totalTrips) {
		this.totalTrips = totalTrips;
	}


	public void setTotalDistance(double totalDistance) {
		this.totalDistance = totalDistance;
	}


	public void setTotalDuration(int totalDuration) {
		this.totalDuration = totalDuration;
	}
	

	public Lista<Trip> getTripsAsociados() {
		return tripsAsociados;
	}


	public void setTripsAsociados(Lista<Trip> tripsAsociados) {
		this.tripsAsociados = tripsAsociados;
	}
	
	
	



	
	public String toString(){
	    
	    return "" + bikeId + ":\t" + totalTrips + " trips\t| " + totalDistance + " metros\t| " + totalDuration + " minutos";
	}
	
	
	
	
	/**
	 * unique id comparator
	 */
	public int compareTo(Bike o){    	
    	return bikeId.compareTo(o.bikeId);        
    }
	
	
	public int compareTo(Bike o, int p) {		
		
		int r = 0;		
		
		// compare by id
		if(p == 1)		
			r = compareTo(o);
		
		//compare by # of trips
		if(p == 2)		
			r = totalTrips.compareTo(o.totalTrips);
		
		//compare by total distance
		if(p == 3)		
			r = totalDistance.compareTo(o.totalDistance);
		
		//compare by total duration
		if(p == 4)		
			r = totalDuration.compareTo(o.totalDuration);
			
		return r;
		
	}


	
	
	
	
	 
	
	
	
	
    
    
    
    
}
