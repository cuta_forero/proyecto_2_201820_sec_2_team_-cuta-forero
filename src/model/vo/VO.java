package model.vo;


/**
 * funcionalidades genericas de un VO
 * @author Andres Forero, Bryan Cuta
 *
 * @param <T> tipo generico de dato Vo
 */
public interface VO<T> extends Comparable<T> {
	
	public int compareTo(T vo, int p);

}
