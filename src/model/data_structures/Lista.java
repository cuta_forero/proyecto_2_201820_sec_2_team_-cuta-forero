package model.data_structures;

import java.util.NoSuchElementException;



/**
 * Estrctura de Datos Lista simple encadenada con puntero al comienzo al final de la lista
 * 
 * @author Andres Forero, Bryan Cuta
 *
 * @param <E> tipo generico de dato 
 */
public class Lista<E>{

	
	
	private Nodo<E> primero;	
	private Nodo<E> ultimo;	
	int size;
	
		
	public Lista()
	{
		primero = ultimo = null;
		size = 0;
	}
	
	public boolean isEmpty() {		
		return primero == null;
	}

	
	
	
	
	public int size() {		
		return size;
	}
	
	
	
	
	
	public void addFirst(E item)
	{
		if(isEmpty())
		{
			primero = ultimo = new Nodo<E>(item);
		}		
		else
		{
			primero = new Nodo<E>(item, primero);
		}		
		size++;
	}
	
	
	
	
	
	public void addEnd(E item)
	{
		if(isEmpty())
			primero = ultimo = new Nodo<E>(item);
		else
			ultimo = ultimo.next = new Nodo<E>(item);
		
		size++;
	}
	
	
	
	
	public E removeFirst()
	{
		if(isEmpty())
			throw new NoSuchElementException();		
		E antiguoPrimero = primero.item;
		
		if(primero == ultimo){
			primero = ultimo = null;
		}
		else{
			primero = primero.next;
		}
		size--;
		return antiguoPrimero;		
	}
	
	
	
	
	public E removeEnd()
	{
		if(isEmpty())
			throw new NoSuchElementException();
		
		E antiguoUltimo = ultimo.item;
		
		if(primero == ultimo)
			primero = ultimo = null;
		else{
			
			Nodo<E> actual = primero;
			
			while(actual.next != ultimo)
			{
				actual = actual.next;
			}
			
			ultimo = actual;
			actual.next = null;				
		}		
		size--;
		
		return antiguoUltimo;		
	}
	
	
	
	
	public E getFirst() {
		return primero.item;
	}


	
	public E getLast() {
		return ultimo.item;
	}

	
	public void deleteList() {
		primero = ultimo = null;
		size = 0;
		
	}
	
	
	public E get(int k) {
	
		//TODO hacer con iterador basado en Nodos
		return null;
	}
	
	
	
	
	public boolean contains(E item) {
		//TODO hacer con iterador basado en Nodos
		return false;
	}
	
	
	
	public Nodo<E> getFirstNodo()
	{
		return primero;
	}
	
	
	public Nodo<E> getLastNodo()
	{
		return ultimo;
	}

	
	
	
	public void print()
	{
		Nodo<E> current = primero;
		while(current.next != null)
		{
			System.out.println(current.item.toString());
			current = current.next;
		}
		System.out.println(current.item.toString());
	}
	
	
	
	
		
	
	
	
	
	
	
	
	
	


	
	
}
