package model.data_structures;

/**
 * clase que representa una estructura de pila y que extiende de ListaDobleEncadenada, que a su vez implementa IList.
 * por consiguiente Stack implementa indirectamente IList
 * 
 * @author Andres Forero, Bryan Cuta
 *
 * @param <E>: objeto Generico
 */
public class Stack<E> extends Lista<E>{

	
	
	
	
	/**
	 * constructor de una pila, por defecto vacia
	 */
	public Stack()
	{
		
		super();
	}
	
	
	/**
	 * agrega un elemento en el tope/final de la pila
	 */
	public void push(E item)
	{
		super.addEnd(item);
	}
	
	
	/**
	 * elimina el ultimo elemento agregado al tope/final de la pila y retorna su valor
	 * @param item
	 */
	public E pop()
	{
		return super.removeEnd();
	}
	
	
	/**
	 * 
	 *  
	 * @return
	 */
	public E peek()
	{
		return super.getLast();
	}

}
