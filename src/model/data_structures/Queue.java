package model.data_structures;

/**
 * clase que representa una estructura de Cola y que extiende de ListaDobleEncadenada que a su vez implementa IList,
 * por consiguiente Queue implementa indirectamente IList
 * 
 * @author Andres Forero, Bryan Cuta
 *
 * @param <E>: objeto Generico
 */
public class Queue<E> extends Lista<E>{

	
	
	
	/**
	 * constructor de una cola, por defecto vacia
	 */
	public Queue()
	{
		super();
	}	
	
	
	/**
	 * agrega/indexa un elemento al final de la cola
	 * @param item
	 */
	public void queue(E item)
	{
		super.addEnd(item);
	}
	
	
	/**
	 * elimina el primer elemento de la cola y retorna su valor
	 */
	public E dequeue()
	{
		return super.removeFirst();
	}
	
	
	
}
